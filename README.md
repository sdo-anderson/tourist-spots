# tourist-spots-service

[![pipeline status](https://gitlab.com/sdo-anderson/tourist-spots/badges/develop/pipeline.svg)](https://gitlab.com/sdo-anderson/tourist-spots/commits/develop)
[![coverage report](https://gitlab.com/sdo-anderson/tourist-spots/badges/develop/coverage.svg)](https://gitlab.com/sdo-anderson/tourist-spots/commits/develop)

![GitHub package.json version](https://img.shields.io/github/package-json/v/sdo-anderson/tourist-spots-service)
![Snyk Vulnerabilities for GitHub Repo](https://img.shields.io/snyk/vulnerabilities/github/sdo-anderson/tourist-spots-service)

# Tourist Spots

## Service for consult tourist spots
